# ics-ans-role-xrdp

Ansible role to install xrdp.

## Role Variables

```yaml
xrdp_firewall_on: setup  a user based firewall script if set to true (the default)
xrdp_wm_path: # define which desktop env is started by xrdp
xrdp_presistent_sessions: false # activate persistent/resumable sessions

xrdp_firewall_http_dest:  # list of websites allowed by the user based firewall rules (both http and https)

```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-xrdp
```

## License

BSD 2-clause
