#!/bin/bash

PROMETHEUS_FILE=/var/lib/node_exporter/xrdp_sessions.prom


echo '' > $PROMETHEUS_FILE 
while read -r a 
do

	USER=$(echo $a |cut -d '+' -f 1)
	DISP=$(echo $a |cut -d '+' -f 2 )
	DISP_ID=$(echo $DISP |sed -e 's/://g')
	DURATION=$(DISPLAY=$DISP xprintidle) # in millisec

	# check if session is active: -a=AND -U=unix -n=noDNS -w=nowarning
  NB_OPEN_SOCK=$(lsof  -a -u $RDPUSER -U -n -w  2>/dev/null  |grep xrdp |grep xrdp_display_$DISP_ID |wc -l)
	
	if [ $NB_OPEN_SOCK -gt 1 ]
	then
		ACTIVE=1
		SES_STATUS=active
	else
		ACTIVE=0
		SES_STATUS=disconnected
	fi

  echo "Xsession idled (days):  $(( $DURATION/1000/60/60/24 )) $USER display=$DISP_ID active=$ACTIVE" |column -t
  # echo "xrdp_sessions{xrdp_user=\"$USER\",xrdp_display=\"$DISP_ID\",xrdp_session_duration_sec=\"$(( DURATION/1000 ))\",xrdp_session_status=\"$SES_STATUS\"} 0" >> $PROMETHEUS_FILE
  echo "xrdp_sessions{xrdp_user=\"$USER\",xrdp_display=\"$DISP_ID\",xrdp_session_duration_sec=\"$(( DURATION/1000 ))\",xrdp_session_status=\"$SES_STATUS\"} $ACTIVE" >> $PROMETHEUS_FILE

done <<< $(ps ax -o user:32,args | grep Xorg | grep -v 'grep\|sed\|root\|csi' |awk '{ print $1 "+" $3 }') |sort -n -k 4
